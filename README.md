# Sample Flask App with CD

This project is a sample Flask app that uses Gitlab for Continuous Deployment. The code, when committed to Gitlab, is automatically deployed to an AWS EC2 instance.
Testing has not yet been implemented.

## Technical Specifications

The app contains a python file [app.py](app.py) which uses Flask to run a server. It is a basic Hello World app. It needs to have Flask and Redis pre-installed on the host machine.
Gitlab YAML ([.gitlab-ci.yml](.gitlab-ci.yml)) file is used to deploy the app. It has just one stage (deploy) as of now.

###### .gitlab-ci.yml script
```yml
- apk update
- apk add openssh
- apk add sshpass
- sshpass -p "$SSH_PASSWORD" scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no deploy.sh "$VM_IP:/tmp/."
- sshpass -p "$SSH_PASSWORD" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "$VM_IP" "bash /tmp/deploy.sh"
```
The Gitlab Runner is running on an AWS EC2 instance and uses the Docker executer to execute the script. Here, the Linux alpine image is used. 
+ In the script, firstly two packages are installed:
  + openssh: to ssh into the instance.
  + sshpass: to provide the password interactively to ssh.  
  `$SSH_PASSWORD` and `$VM_IP` are Gitlab's secret variables and are automatically supplied by Gitlab during execution of the script.  
  Options in ssh to remove scrict host checking is there to bypass the prompt made by ssh from the script.
+ The [deploy.sh](deploy.sh) file is then copied onto the server using scp.
+ Then, this script is run using ssh on the server.

###### deploy.sh
The script to deploy the app on the server. 
```bash
cd ~/ci-test
git pull origin master
pid=$(cat pid.txt)
echo $pid
ps -e | grep $pid && kill $pid
nohup python3 app.py > /dev/null &
pid=$(echo $!)
echo $pid
echo $pid | cat > pid.txt
```
It does the following things:
+ Updated code is pulled from gitlab (directory for which has already been set up).
+ Process ID of the the app that was run last time is extracted from the file `pid.txt` (where we store the PID).
+ If the process with that `pid` is running, it is terminated.
+ A new process is created that runs the app (with nohup and in the background)
+ The PID of this prpcess is stored in the `pid.txt` file.

The app has a downtime of about 0.5 seconds due to the fact that the process is stopped and re-run.

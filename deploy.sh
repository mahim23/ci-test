# docker rm $(docker stop cname)
# docker image rm mahim23/ci-test:1.0.0
# docker run -d -p 80:80 --name cname mahim23/ci-test:1.0.0

cd ~/ci-test
git pull origin master
pid=$(cat pid.txt)
echo $pid
ps -e | grep $pid && kill $pid
nohup python3 app.py > /dev/null &
pid=$(echo $!)
echo $pid
echo $pid | cat > pid.txt
